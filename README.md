Explanation of featurtes
We have three main screens

Login/ Register Screen
- Login Modal
- Register Modal
- Authentification routes using tokens which prevent users needing to login if they are already logged in


Dashboard
- Displays a list of the users current presentations
- A logout button
- A settings button which opens a modal allowing users to turn autosave on/off
- Button leading to a modal allowing users to create a new presentation, they can input files as thumbnails,   text for a description and a title
- presentation card
- Presentation Screen



Header:
- Logout button
- Back to dashboard button
- Delete presentation button
- Save presentation button,
- Preview presentation button
- Rearrange slides button
- Edit title
- Edit thumbnail
- Revisison history button
- The preview button creates a new page where the user can
- view the slideshow on a full screen.



Toolbar:
- Adding slide
- Removing slide
- Add textbox
- Add Image
- Add code
- Adding presentation background
- Adding slide background
- Adding transitions between slides
- Adding Video
- Text, Images, Video and Code can all be dragged to different positions on the slides,
dragged for resizing, edited with another modal by double clicking and deleted with a right click

